<?php

namespace PBK;

use Klein;

/**
 * Framework
 * Framework is a super simple micro mvc framework. Routing is provided by Klein so it should be pretty fast.
 */
class Framework {

    public $klein;

    /**
     * Get the application ready for deployment, this won't actually do anything visible.
     */
    public function __construct() {
        // Register klein
        $this->klein = new Klein\Klein();
    }

    /**
     * Visibly start the Frame engine
     */
    public function start() {
        
    }

    /**
     * Stop the app and clean it up.
     */
    public function stop() {
        $this->klein->dispatch();
    }

}
